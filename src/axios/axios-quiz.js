import axios from 'axios'

export default axios.create({
    baseURL: 'https://react-quiz-36730.firebaseio.com/'
})